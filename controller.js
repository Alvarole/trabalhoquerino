$(document).ready(function () {
    // IMPORTS e DEFINIÇÃO DE VARIAVEIS
    const { remote } = require('electron');
    const app = require('electron').remote.app;
    const { promisify } = require('util');
    var redis = require("redis"), client = redis.createClient();

    var produtos;
    var lote;
    var dta_adicao;
    var preco;
    var quantidade;

    // JANELA
    $('#minimizar').click(function () {
        remote.BrowserWindow.getFocusedWindow().minimize();
    });
    $('#maximizar').click(function () {
        if (remote.BrowserWindow.getFocusedWindow().isMaximized()) {
            remote.BrowserWindow.getFocusedWindow().restore();
        } else {
            remote.BrowserWindow.getFocusedWindow().maximize();
        }
    });
    $('#fechar').click(function () {
        client.quit();
        remote.BrowserWindow.getFocusedWindow().close();
    });

    // BOTÃO DE ADD
    $('#create').click(function () {
        if ($('#n_lote')[0].value == "" || $('#nome_produto')[0].value == "" || $('#quantidade')[0].value == "" || $('#preco')[0].value == "") {
            alert("Preencha todos os campos!");
        } else {
            if (localStorage.length === 0) {
                verificarPNum();
                location.reload();
            } else {
                updateValues(localStorage.getItem('update'));
                localStorage.clear()
                alert("Produto atualizado com sucesso!")
                location.reload();
            }
        }
    });

    // READ
    readValues();

    function makeDateNow() {
        let year = new Date().getFullYear();
        let month = new Date().getMonth();
        let day = new Date().getDate();
        return (day + "/" + (month + 1) + "/" + year + " - " + Date.now());
    }

    function verificarPNum() {
        client.on("error", function (err) {
            console.log("Error " + err);
        });
        const hlenAsync = promisify(client.hlen).bind(client);
        hlenAsync("lote").then(function (res) {
            createValue(res);
        });
    }

    function readValues() {
        var valor_total = new Array;
        client.on("error", function (err) {
            console.log("Error " + err);
        });
        const hValsAsync = promisify(client.hvals).bind(client);
        hValsAsync("lote").then(function (res) {
            this.lote = res;
        });
        hValsAsync("produtos").then(function (res) {
            this.produtos = res;
        });
        hValsAsync("quantidade").then(function (res) {
            this.quantidade = res;
        });
        hValsAsync("data").then(function (res) {
            this.dta_adicao = res;
        });
        hValsAsync("preco").then(function (res) {
            this.preco = res;

            let tableRef = document.getElementById("my-table");

            for (let index = 0; index < this.quantidade.length; index++) {
                let newRow = tableRef.insertRow(1);

                let cell1 = newRow.insertCell(0);
                let cell2 = newRow.insertCell(1);
                let cell3 = newRow.insertCell(2);
                let cell4 = newRow.insertCell(3);
                let cell5 = newRow.insertCell(4);
                let cellButtonDel = newRow.insertCell(5);
                let cellButtonUpdate = newRow.insertCell(6);

                let newText = document.createTextNode(this.lote[index]);
                let newText2 = document.createTextNode(this.produtos[index]);
                let newText3 = document.createTextNode(this.quantidade[index]);
                let newText4 = document.createTextNode(this.dta_adicao[index]);
                let newText5 = document.createTextNode(this.preco[index]);
                valor_total.push((Number(this.quantidade[index]) * Number(this.preco[index]).toFixed(2)))

                let buttonDell = document.createElement("div");
                buttonDell.setAttribute("id", index)
                buttonDell.innerHTML = "<div class='btn-group'> <button class='btn-white btn btn-xs' id='delete'>Delete</button>";

                let buttonUpdate = document.createElement("div");
                buttonUpdate.setAttribute("id", index)
                buttonUpdate.innerHTML = "<div class='btn-group'> <button class='btn-white btn btn-xs' id='update'>Atualizar</button>";

                cell1.appendChild(newText);
                cell2.appendChild(newText2);
                cell3.appendChild(newText3);
                cell4.appendChild(newText4);
                cell5.appendChild(newText5);

                cellButtonDel.appendChild(buttonDell);
                cellButtonUpdate.appendChild(buttonUpdate);

                buttonDell.onclick = function () {
                    client.on("error", function (err) {
                        console.log("Error " + err);
                    });

                    deleteValues(buttonDell.id);
                    location.reload();
                };

                buttonUpdate.onclick = function () {
                    client.on("error", function (err) {
                        console.log("Error " + err);
                    });

                    $("#n_lote")[0].value = cell1.textContent
                    $("#nome_produto")[0].value = cell2.textContent
                    $("#quantidade")[0].value = cell3.textContent
                    $("#preco")[0].value = cell5.textContent

                    localStorage.setItem('update', buttonUpdate.id)
                };

                somaValor(valor_total);
            }
        });
        
    }

    function createValue(contador_registros) {
        client.on("error", function (err) {
            console.log("Error " + err);
        });
        client.hset("lote", "l_" + Number(contador_registros + 1), $('#n_lote')[0].value);
        client.hset("produtos", "p_" + Number(contador_registros + 1), $('#nome_produto')[0].value);
        client.hset("quantidade", "q_" + Number(contador_registros + 1), $('#quantidade')[0].value);
        client.hset("data", "d_" + Number(contador_registros + 1), makeDateNow());
        client.hset("preco", "rs_" + Number(contador_registros + 1), $('#preco')[0].value);
        alert("Produto criado com sucesso!")
    }

    function deleteValues(id) {
        id = Number(id) + 1;

        client.on("error", function (err) {
            console.log("Error " + err);
        });

        client.hdel("lote", "l_" + id);
        client.hdel("produtos", "p_" + id);
        client.hdel("quantidade", "q_" + id);
        client.hdel("data", "d_" + id);
        client.hdel("preco", "rs_" + id);
        alert("Produto deletado com sucesso!");
    }

    function updateValues(id) {
        id = Number(id) + 1;

        client.on("error", function (err) {
            console.log("Error " + err);
        });

        client.hmset("lote", "l_" + id, $('#n_lote')[0].value)
        client.hmset("produtos", "p_" + id, $('#nome_produto')[0].value);
        client.hmset("quantidade", "q_" + id, $('#quantidade')[0].value);
        client.hmset("data", "d_" + id, makeDateNow());
        client.hmset("preco", "rs_" + id, $('#preco')[0].value);
    }

    function somaValor(arr_valores) {
        var total_valor = 0;
        arr_valores.forEach(element => {
            total_valor += element;
        });
        $('#valor_total')[0].innerHTML = total_valor.toFixed(2);
    }

});
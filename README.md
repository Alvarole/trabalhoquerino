# Mercadinho Universal
Esse trabalho consiste em cumprir com a criação de CRUD com a utilização de Redis.

Para executar o código feito em electron, basta executar:
```
npm install
```
E depois:
```
npm start
```

Para realizar a copilação, basta instalar o electron-packager com:
```
npm i electron-packager
```
e para rodar é necessário usar:
```
electron-packager <sourcedir> <appname> --platform=<platform> --arch=<arch> [optional flags...]
```
Para mais informações, acesse https://www.npmjs.com/package/electron-packager#usage